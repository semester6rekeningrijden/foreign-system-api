package com.teamterry.foreign.system.dtos.journeydatas;


import com.teamterry.foreign.system.dtos.journeydatas.CleanDTO.JourneyCleanDTO;

import java.io.Serializable;

public class JourneyDataDTO {

    private int Id;
    private double longitude;
    private double latitude;
    private String licenseplate;
    private String nss;
    private JourneyCleanDTO journey;

    public JourneyDataDTO() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getLicenseplate() {
        return licenseplate;
    }

    public void setLicenseplate(String licenseplate) {
        this.licenseplate = licenseplate;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public JourneyCleanDTO getJourney() {
        return journey;
    }

    public void setJourney(JourneyCleanDTO journey) {
        this.journey = journey;
    }
}
