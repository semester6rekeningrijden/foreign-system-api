package com.teamterry.foreign.system.dtos.journeys;

import com.teamterry.foreign.system.dtos.journeydatas.JourneyDataDTO;
import com.teamterry.foreign.system.dtos.vehicles.VehicleCleanDto;

import java.io.Serializable;
import java.util.List;

public class JourneyDto {
    private int id;
    private boolean finished;
    private String nss;
    private String vehicle;
    private List<JourneyDataDTO> journeyData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public String getUser() {
        return nss;
    }

    public void setUser(String nss) {
        this.nss = nss;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public List<JourneyDataDTO> getJourneyData() {
        return journeyData;
    }

    public void setJourneyData(List<JourneyDataDTO> journeyData) {
        this.journeyData = journeyData;
    }
}
