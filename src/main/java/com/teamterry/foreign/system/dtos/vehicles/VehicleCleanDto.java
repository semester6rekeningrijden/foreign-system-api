package com.teamterry.foreign.system.dtos.vehicles;

import com.teamterry.foreign.system.dtos.VehicleType;

import java.io.Serializable;

public class VehicleCleanDto {
    private int id;
    private String licensePlate;
    private VehicleType type;
    private String owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
