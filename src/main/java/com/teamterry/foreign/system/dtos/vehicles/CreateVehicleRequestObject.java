package com.teamterry.foreign.system.dtos.vehicles;

import com.teamterry.foreign.system.dtos.VehicleType;

public class CreateVehicleRequestObject {

    private String licensePlate;
    private VehicleType type;
    private Integer ownerId;
    private String nss;

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }
}
