package com.teamterry.foreign.system.dtos.journeydatas;



import com.teamterry.foreign.system.dtos.journeydatas.CleanDTO.VehicleCleanDTO;

import java.io.Serializable;

public class JourneyDTO  {

    private int Id;
    private boolean finished;
    private VehicleCleanDTO vehicle;

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public VehicleCleanDTO getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleCleanDTO vehicle) {
        this.vehicle = vehicle;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
