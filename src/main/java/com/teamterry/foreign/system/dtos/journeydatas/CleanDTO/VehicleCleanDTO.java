package com.teamterry.foreign.system.dtos.journeydatas.CleanDTO;

import java.io.Serializable;

public class VehicleCleanDTO {
    private int Id;
    private String licensePlate;
    private String type;

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
