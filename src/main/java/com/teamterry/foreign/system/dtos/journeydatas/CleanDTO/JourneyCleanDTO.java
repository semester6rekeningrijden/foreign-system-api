package com.teamterry.foreign.system.dtos.journeydatas.CleanDTO;

import java.io.Serializable;

public class JourneyCleanDTO {
    private int Id;
    private boolean finished;
    private VehicleCleanDTO vehicleId;

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public VehicleCleanDTO getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(VehicleCleanDTO vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
