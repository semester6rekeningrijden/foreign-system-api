package com.teamterry.foreign.system.dtos.vehicles;

import com.teamterry.foreign.system.dtos.VehicleType;
import com.teamterry.foreign.system.dtos.journeys.JourneyDto;
import com.teamterry.foreign.system.dtos.users.UserCleanDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VehicleDto {

    private int id;
    private String licensePlate;
    private VehicleType  type;
    private List<JourneyDto> journey;
    private UserCleanDto user;

    public VehicleDto() {
        journey = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public List<JourneyDto> getJourney() {
        return journey;
    }

    public void setJourney(List<JourneyDto> journey) {
        this.journey = journey;
    }

    public UserCleanDto getUser() {
        return user;
    }

    public void setUser(UserCleanDto user) {
        this.user = user;
    }
}
