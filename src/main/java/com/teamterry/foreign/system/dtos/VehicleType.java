package com.teamterry.foreign.system.dtos;

import java.io.Serializable;

public enum VehicleType implements Serializable {
    CAR,
    BUS,
    TRUCK
}
