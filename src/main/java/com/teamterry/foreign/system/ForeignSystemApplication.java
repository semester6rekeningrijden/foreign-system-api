package com.teamterry.foreign.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForeignSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForeignSystemApplication.class, args);
	}

}
