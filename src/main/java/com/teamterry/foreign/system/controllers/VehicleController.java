package com.teamterry.foreign.system.controllers;

import com.google.gson.Gson;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.teamterry.foreign.system.dtos.vehicles.VehicleDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {
    private RestTemplate restTemplate;

    @Value("${registrationApi}")
    private String registrationApi;

    @Autowired
    public VehicleController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @HystrixCommand(fallbackMethod = "fallback")
    @GetMapping("{licensePlate}")
    public ResponseEntity<String> vehicleByLicensePlate(@PathVariable final String licensePlate) {

        try {
            return restTemplate.getForEntity(String.format("%s/%s/%s", registrationApi, "vehicles/license", licensePlate), String.class);
        } catch (HttpClientErrorException e) {
            return new ResponseEntity<String>("Vehicle couldn't be found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<String>("Vehicle couldn't be found", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Object> fallback(String licensePlate) {
        return new ResponseEntity<Object>(String.format("Vehicle Data from licenseplate %s could not be found", licensePlate), HttpStatus.NOT_FOUND);
    }

    private List<HttpMessageConverter<?>> getMessageConverters() {
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(new MappingJackson2HttpMessageConverter());

        return converters;
    }
}
